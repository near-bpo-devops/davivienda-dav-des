#!/bin/bash
# shellcheck disable=SC2046

ulimit -n {{ pillar.get('envoy_max_open_files', '102400') }}
sysctl fs.inotify.max_user_watches={{ pillar.get('envoy_max_inotify_watches', '524288') }}

exec /usr/bin/envoy -c /opt/apigee/etc/envoy/envoy.cfg --restart-epoch $RESTART_EPOCH --service-cluster {{ grains['cluster_name'] }} --service-node {{ grains['service_node'] }} --service-zone {{ grains.get('ec2_availability-zone', 'unknown') }}

# node:
#   cluster: apisdes_davivienda
#   id: apisdes-davivienda-id
