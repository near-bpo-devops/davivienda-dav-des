#!/bin/bash
# shellcheck disable=SC2046

#################### --------------#######################
####### Author: Israel Elías

#Install git
#This command is necesary to pull out all the commands and yo be updated
sudo yum install -y git
#Installation for vim, easier interface
sudo yum install -y vim

#Install docker
#sudo yum-config-manager \
#    --add-repo \
#    https://download.docker.com/linux/rhel/docker-ce.repo


#Get envoy latest version, optional
#docker cp $(docker create envoyproxy/envoy:latest):/usr/local/bin/envoy .
#install jq and yq