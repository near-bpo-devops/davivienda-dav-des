#/bin/sh
if [ "$#" -eq "0" ] 
then {
    echo "usage: $0 <Git File>"
    return 1
}
fi

#Variables
DESKTOP_CLIENT_ID="999999999999-jhygbgtmnjseksse5ld37t0q7gpnbmus.apps.googleusercontent.com"
DESKTOP_CLIENT_SECRET="gppBi8u3zGe6ZQtxsecretoc"
IAP_LOAD_BALANCER_CLIENT_SECRET="999999999999-rku4oaefkk2q9euedp7a5857gb2u1npd.apps.googleusercontent.com"

#autenticacion y generacion de Token

if [ -f ~/.gitd/rt ]
then {
      # Si ya tenemos un token, lo leemos y lo usamos.
      REFRESH_TOKEN=$(head -1 ~/.gitd/rt)
}
fi

if [ "$REFRESH_TOKEN" = "" ]
then 
      echo "Ingrese el token entregado en la siguiente URL: \nhttps://accounts.google.com/o/oauth2/v2/auth?client_id=$DESKTOP_CLIENT_ID&response_type=code&scope=openid%20email&access_type=offline&redirect_uri=urn:ietf:wg:oauth:2.0:oob"
      read -p 'AUTH CODE: ' AUTH_CODE

      if [ "$AUTH_CODE" = "" ]
      then 
            echo "Token could not be empty!"
            return 2
      fi
      #echo "AUTH_CODE: $AUTH_CODE"

      REFRESH_TOKEN=$(curl -s --data client_id=$DESKTOP_CLIENT_ID \
            --data client_secret=$DESKTOP_CLIENT_SECRET \
            --data code=$AUTH_CODE \
            --data redirect_uri=urn:ietf:wg:oauth:2.0:oob \
            --data grant_type=authorization_code \
            https://oauth2.googleapis.com/token | grep -Po '"refresh_token": "*\K[^"]*') 
      # echo "REFRESH_TOKEN: $REFRESH_TOKEN"

      if [ ! -d ~/.gitd ] 
      then 
            # Creamos el directorio para persistencia
            mkdir ~/.gitd 

            # Si falla, abandonamos el script
            if [ $? -gt 0 ]
            then  
                  echo "Store-token process failed" 
                  return 3
            fi
      fi

      # Almacenamos el token recibido
      echo $REFRESH_TOKEN > ~/.gitd/rt
else
      echo "Usando el token de refresco almacenado"
fi

BEARER_DAVIVIENDA=$(curl -s --data client_id=$DESKTOP_CLIENT_ID \
      --data client_secret=$DESKTOP_CLIENT_SECRET \
      --data refresh_token=$REFRESH_TOKEN \
      --data grant_type=refresh_token \
      --data audience=$IAP_LOAD_BALANCER_CLIENT_SECRET \
      https://oauth2.googleapis.com/token | grep -Po '"id_token": "*\K[^"]*') 
#echo "$BEARER_DAVIVIENDA"

#Copiado del repositorio
git clone -c http.extraheader="Proxy-Authorization: Bearer $BEARER_DAVIVIENDA" $1